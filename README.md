![Tripetto](https://docs.tripetto.community/assets/header.svg)

Tripetto is a full-fledged form kit. Rapidly create and deploy smart flowing forms and surveys. Drop the kit in your codebase and use all of it, or just the parts you need. The visual [**editor**](https://www.npmjs.com/package/tripetto) is for form creation, the [**collector**](https://www.npmjs.com/package/tripetto-collector) for response collection and the [**SDK**](https://docs.tripetto.community/blocks) for developing more form building blocks.

# Plain JavaScript example
[![pipeline status](https://gitlab.com/tripetto/examples/plain/badges/master/pipeline.svg)](https://gitlab.com/tripetto/examples/plain/commits/master)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![docs](https://img.shields.io/badge/docs-website-blue.svg)](https://docs.tripetto.community/collector)
[![Join the community on Spectrum](https://withspectrum.github.io/badge/badge.svg)](https://spectrum.chat/tripetto)

This demo shows how to implement the collector for some basic form input controls using just plain JavaScript. It has no fancy markup or styles. It shows the minimal required code to get it up and running.

# How to run
1. [Download](https://gitlab.com/tripetto/examples/plain/repository/master/archive.zip) or clone the [repository](https://gitlab.com/tripetto/examples/plain) to your local machine:
```bash
$ git clone https://gitlab.com/tripetto/examples/plain.git
```

2. Run `npm install` inside the downloaded/cloned folder:
```bash
$ npm install
```

3. Start the test server and open the URL `http://localhost:9000` in the browser of your choice to show the form:
```bash
$ npm test
```

4. Open another terminal/command prompt and start the editor (this will open an example definition file located at `./static/demo.json`):
```bash
$ npm start
```

This last command will probably automatically open your default browser with the URL http://localhost:3333. If not, open the browser of your choice and navigate to this URL.

When you have changed your form, click the `Save` button at the right top of the editor. Then, refresh the form collector to run the altered form.

# Documentation
The complete Tripetto documentation can be found at [docs.tripetto.community](https://docs.tripetto.community).

# Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/examples/plain/issues) and we'll look into them.

For general support contact us at [support@tripetto.com](mailto:support@tripetto.com). We're more than happy to assist you.

# License
Have a blast. [MIT](https://opensource.org/licenses/MIT).

# About us
If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
