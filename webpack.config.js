const webpack = require("webpack");
const path = require("path");

module.exports = {
    entry: "./src/index.ts",
    output: {
        filename: "bundle.js",
        path: __dirname + "/static"
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                use: "ts-loader"
            }
        ]
    },
    resolve: {
        extensions: [".ts", ".js"],
        mainFields: ["browser", "main"]
    },
    devServer: {
        contentBase: path.join(__dirname, "static"),
        port: 9000,
        host: "0.0.0.0"
    }
};
