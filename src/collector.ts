import * as Tripetto from "tripetto-collector";

export class DOMCollector extends Tripetto.Collector<HTMLElement> {
    /** We need to store some references per instance. */
    private props: {
        [instance: string]:
            | {
                  /** Contains the form. */
                  Form: HTMLFormElement;

                  /** Contains the form fields. */
                  Content: HTMLElement;

                  /** Contains the `Next`-button. */
                  Next: HTMLButtonElement;

                  /** Contains the `PRevious`-button. */
                  Previous: HTMLButtonElement;

                  /** Contains the progress indicator. */
                  Progress: HTMLSpanElement;

                  /** Contains the action reference. */
                  Action: Tripetto.Await | undefined;
              }
            | undefined;
    } = {};

    /** A new instance is started. */
    public OnInstanceStart(instance: Tripetto.Instance): void {
        const form = document.createElement("form");
        const title = document.createElement("h1");
        const buttonPrevious = document.createElement("button");
        const buttonNext = document.createElement("button");
        const progress = document.createElement("span");
        const content = document.createElement("section");

        /** Title of the form. */
        title.textContent = this.Ontology ? this.Ontology.Name : "";

        /** Button: Previous step. */
        buttonPrevious.textContent = " Back";
        buttonPrevious.setAttribute("type", "button");
        buttonPrevious.setAttribute("disabled", "disabled");
        buttonPrevious.addEventListener("click", () => {
            const props = this.props[instance.Hash];

            if (props && props.Action) {
                props.Action.Cancel();
            }
        });

        /** Button: Next step. */
        buttonNext.textContent = "Next ";
        buttonNext.setAttribute("type", "button");
        buttonNext.setAttribute("disabled", "disabled");
        buttonNext.addEventListener("click", () => {
            const props = this.props[instance.Hash];

            if (props && props.Action) {
                props.Action.Done();
            }
        });

        /** Progress percentage. */
        progress.textContent = "0%";

        /** Add the elements to the form. */
        form.appendChild(title);
        form.appendChild(content);
        form.appendChild(buttonPrevious);
        form.appendChild(buttonNext);
        form.appendChild(progress);

        /** Add the form to the DOM. */
        (document.getElementById("collector") || document.body).appendChild(form);

        /** Multiple instances can run, so we need to store references per instance. */
        this.props[instance.Hash] = {
            Form: form,
            Content: content,
            Next: buttonNext,
            Previous: buttonPrevious,
            Progress: progress,
            Action: undefined
        };
    }

    /** An instance ends. */
    public OnInstanceEnd(instance: Tripetto.Instance, type: "ended" | "stopped" | "paused"): void {
        const props = this.props[instance.Hash];

        /** Remove form. */
        if (props && props.Form) {
            props.Form.remove();
        }

        /** Destroy instance references. */
        this.props[instance.Hash] = undefined;

        if (type === "ended") {
            // Output the collected data to the console
            console.dir(instance.Values);

            alert("Your form is completed! Now watch the collected data in your browser console.");
        }
    }

    /** Something needs to be rendered to the context. */
    public OnInstanceRender(
        instance: Tripetto.Instance,
        what: Tripetto.Cluster<HTMLElement> | Tripetto.Node<HTMLElement>,
        action: Tripetto.Await
    ): void {
        const props = this.props[instance.Hash];

        if (!props) {
            return;
        }

        if (what instanceof Tripetto.Cluster) {
            props.Action = action;

            props.Content.appendChild(document.createElement("div"));
        } else {
            const container = props.Content.firstElementChild as HTMLElement;

            if (what.Block) {
                // Render a block
                container.appendChild(what.Block.OnRender(instance, action));
            } else {
                // Render a static item
                const div = document.createElement("div");

                if (what.Props.NameVisible && what.Props.Name) {
                    const title = document.createElement("h3");

                    title.textContent = what.Props.Name;

                    div.appendChild(title);
                }

                if (what.Props.Description) {
                    const p = document.createElement("p");

                    p.textContent = what.Props.Description;

                    div.appendChild(p);
                }

                container.appendChild(div);
            }
        }
    }

    /** Context needs to be cleaned up. */
    public OnInstanceUnrender(instance: Tripetto.Instance, direction: "forward" | "backward"): void {
        const props = this.props[instance.Hash];

        if (!props) {
            return;
        }

        this.props.Actions = undefined;

        const container = props.Content.firstElementChild;

        if (container) {
            container.remove();
        }
    }

    /** The instance makes a step. */
    public OnInstanceStep(instance: Tripetto.Instance, direction: "forward" | "backward"): void {
        const props = this.props[instance.Hash];

        if (!props) {
            return;
        }

        props.Next.textContent = `${instance.IsAtEnd ? "Complete" : "Next"} `;
        props.Previous.disabled = instance.Steps === 0;
    }

    /** The instance is validated. */
    public OnInstanceValidated(instance: Tripetto.Instance, result: "fail" | "pass"): void {
        const props = this.props[instance.Hash];

        if (!props) {
            return;
        }

        props.Next.disabled = result === "fail";
    }

    /** Instance progress changed. */
    public OnInstanceProgress(instance: Tripetto.Instance, percentage: number): void {
        const props = this.props[instance.Hash];

        if (!props) {
            return;
        }

        props.Progress.textContent = `${Math.floor(percentage)}%`;
    }
}
