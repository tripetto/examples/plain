import "./blocks";
import * as Superagent from "superagent";
import { DOMCollector } from "./collector";

// Fetch our form and start a `DOMCollector` instance.
Superagent.get("demo.json").end((error: {}, response: Superagent.Response) => {
    if (response.ok) {
        const collector = new DOMCollector(response.text);

        collector.Start();
    } else {
        alert("Bummer! Cannot load form definition. Does the file exists?");
    }
});
