import * as Tripetto from "tripetto-collector";
import { IRadiobutton, IRadiobuttons } from "tripetto-block-radiobuttons";
import { Static } from "../static";

@Tripetto.node("tripetto-block-radiobuttons")
export class Radiobuttons extends Tripetto.NodeBlock<HTMLElement, IRadiobuttons> {
    private Update(data: Tripetto.Data<string>, id: string | undefined): void {
        const value = Tripetto.F.FindFirst(this.Props.Radiobuttons, (radiobutton: IRadiobutton) => radiobutton.Id === id);

        data.Set(value ? value.Value || value.Name : value, id);
    }

    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const button = this.DataAssert<string>(instance, "button");
        const rendering = document.createElement("div");
        const field = document.createElement("div");
        const selected =
            Tripetto.F.FindFirst(this.Props.Radiobuttons, (radiobutton: IRadiobutton) =>
                Tripetto.F.CastToBoolean(button.Reference === radiobutton.Id)
            ) || Tripetto.F.ArrayItem(this.Props.Radiobuttons, 0);

        if (selected) {
            this.Update(button, selected.Id);
        }

        Tripetto.F.Each(this.Props.Radiobuttons, (radiobutton: IRadiobutton) => {
            const label = document.createElement("label");
            const input = document.createElement("input");

            input.setAttribute("type", "radio");
            input.name = `radiobuttons-${this.Node.Props.Id}`;
            input.id = `radiobutton-${radiobutton.Id}`;

            if (selected === radiobutton) {
                input.checked = true;
            }

            input.addEventListener("propertychange", () => this.Update(button, radiobutton.Id));
            input.addEventListener("change", () => this.Update(button, radiobutton.Id));
            input.addEventListener("blur", () => this.Update(button, radiobutton.Id));

            label.appendChild(input);
            label.appendChild(document.createTextNode(radiobutton.Name));
            field.appendChild(label);
        });

        rendering
            .appendChild(
                Static(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    false,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(field);

        return rendering;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        return true;
    }
}
