import * as Tripetto from "tripetto-collector";
import { IText } from "tripetto-block-text";
import { Static } from "../static";
import "./condition";

@Tripetto.node("tripetto-block-text")
export class Text extends Tripetto.NodeBlock<HTMLElement, IText> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const slot = this.SlotAssert<Tripetto.Slots.TextSlot>("value");
        const value = this.DataAssert<string>(instance, slot);
        const rendering = document.createElement("div");
        const input = document.createElement("input");

        input.setAttribute("type", "text");
        input.setAttribute("id", this.Node.Props.Id);

        if (Tripetto.F.IsFilledString(this.Node.Props.Placeholder)) {
            input.setAttribute("placeholder", this.Node.Props.Placeholder);
        }

        input.value = value.Value;

        input.addEventListener("propertychange", () => (value.Value = input.value));
        input.addEventListener("change", () => (value.Value = input.value));
        input.addEventListener("click", () => (value.Value = input.value));
        input.addEventListener("keyup", () => (value.Value = input.value));
        input.addEventListener("input", () => (value.Value = input.value));
        input.addEventListener("paste", () => (value.Value = input.value));
        input.addEventListener("blur", () => (input.value = value.String));

        rendering
            .appendChild(
                Static(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    slot.Required,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(input);

        return rendering;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert<Tripetto.Slots.TextSlot>("value");
        const value = this.DataAssert<string>(instance, slot);

        return !slot.Required || value.Value !== "";
    }
}
