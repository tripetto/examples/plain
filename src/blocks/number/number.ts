import * as Tripetto from "tripetto-collector";
import { INumber } from "tripetto-block-number";
import { Static } from "../static";

@Tripetto.node("tripetto-block-number")
export class Number extends Tripetto.NodeBlock<HTMLElement, INumber> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const slot = this.SlotAssert("number");
        const value = this.DataAssert<number>(instance, slot);
        const rendering = document.createElement("div");
        const input = document.createElement("input");

        input.setAttribute("type", "number");
        input.setAttribute("id", this.Node.Props.Id);

        if (Tripetto.F.IsFilledString(this.Node.Props.Placeholder)) {
            input.setAttribute("placeholder", this.Node.Props.Placeholder);
        }

        input.value = value.String;

        input.addEventListener("propertychange", () => (value.Data = input.value));
        input.addEventListener("change", () => (value.Data = input.value));
        input.addEventListener("click", () => (value.Data = input.value));
        input.addEventListener("keyup", () => (value.Data = input.value));
        input.addEventListener("input", () => (value.Data = input.value));
        input.addEventListener("paste", () => (value.Data = input.value));
        input.addEventListener("blur", () => (input.value = value.String));

        rendering
            .appendChild(
                Static(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    slot.Required,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(input);

        return rendering;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("number");
        const value = this.DataAssert<number>(instance, slot);

        return !slot.Required || value.String !== "";
    }
}
