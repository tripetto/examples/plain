import * as Tripetto from "tripetto-collector";
import { IURL } from "tripetto-block-url";
import { Static } from "../static";

/* tslint:disable-next-line:max-line-length */
const IS_URL = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;

@Tripetto.node("tripetto-block-url")
export class URL extends Tripetto.NodeBlock<HTMLElement, IURL> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const slot = this.SlotAssert("url");
        const url = this.DataAssert<string>(instance, slot);
        const rendering = document.createElement("div");
        const input = document.createElement("input");

        input.setAttribute("type", "url");
        input.setAttribute("id", this.Node.Props.Id);

        if (Tripetto.F.IsFilledString(this.Node.Props.Placeholder)) {
            input.setAttribute("placeholder", this.Node.Props.Placeholder);
        }

        input.value = url.Value;

        input.addEventListener("propertychange", () => (url.Value = input.value));
        input.addEventListener("change", () => (url.Value = input.value));
        input.addEventListener("click", () => (url.Value = input.value));
        input.addEventListener("keyup", () => (url.Value = input.value));
        input.addEventListener("input", () => (url.Value = input.value));
        input.addEventListener("paste", () => (url.Value = input.value));
        input.addEventListener("blur", () => (input.value = url.Value));

        rendering
            .appendChild(
                Static(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    slot.Required,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(input);

        return rendering;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("url");
        const url = this.DataAssert<string>(instance, slot);

        if (slot.Required && url.Value === "") {
            return false;
        }

        if (url.Value !== "" && !IS_URL.test(url.Value)) {
            return false;
        }

        return true;
    }
}
