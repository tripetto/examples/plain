import * as Tripetto from "tripetto-collector";
import { IPassword } from "tripetto-block-password";
import { Static } from "../static";

@Tripetto.node("tripetto-block-password")
export class Password extends Tripetto.NodeBlock<HTMLElement, IPassword> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const slot = this.SlotAssert("password");
        const password = this.DataAssert<string>(instance, slot);
        const rendering = document.createElement("div");
        const input = document.createElement("input");

        input.setAttribute("type", "password");
        input.setAttribute("id", this.Node.Props.Id);

        if (Tripetto.F.IsFilledString(this.Node.Props.Placeholder)) {
            input.setAttribute("placeholder", this.Node.Props.Placeholder);
        }

        input.value = password.Value;

        input.addEventListener("propertychange", () => (password.Value = input.value));
        input.addEventListener("change", () => (password.Value = input.value));
        input.addEventListener("click", () => (password.Value = input.value));
        input.addEventListener("keyup", () => (password.Value = input.value));
        input.addEventListener("input", () => (password.Value = input.value));
        input.addEventListener("paste", () => (password.Value = input.value));
        input.addEventListener("blur", () => (input.value = password.String));

        rendering
            .appendChild(
                Static(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    slot.Required,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(input);

        return rendering;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("password");
        const password = this.DataAssert<string>(instance, slot);

        return !slot.Required || password.Value !== "";
    }
}
