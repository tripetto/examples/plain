import * as Tripetto from "tripetto-collector";
import { ICheckbox } from "tripetto-block-checkbox";
import "./conditions";

@Tripetto.node("tripetto-block-checkbox")
export class Checkbox extends Tripetto.NodeBlock<HTMLElement, ICheckbox> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const slot = this.SlotAssert("checked");
        const checkbox = this.DataAssert<boolean>(instance, slot);
        const label = document.createElement("label");
        const input = document.createElement("input");

        input.setAttribute("type", "checkbox");
        input.checked = checkbox.Value;

        input.addEventListener("propertychange", () => (checkbox.Value = input.checked));
        input.addEventListener("change", () => (checkbox.Value = input.checked));
        input.addEventListener("click", () => (checkbox.Value = input.checked));

        label.appendChild(input);
        label.appendChild(document.createTextNode(this.Node.Props.Name + (slot.Required ? "*" : "")));

        return label;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("checked");
        const checkbox = this.DataAssert<boolean>(instance, slot);

        return !slot.Required || checkbox.Value;
    }
}
