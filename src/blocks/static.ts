export function Static(id: string, name: string, required: boolean | undefined, description: string, explanation: string): HTMLDivElement {
    const container = document.createElement("div");

    if (name) {
        const label = document.createElement("label");

        label.textContent = name + " ";
        label.setAttribute("for", id);

        if (required) {
            const asterisk = document.createElement("span");

            asterisk.textContent = "* ";

            label.appendChild(asterisk);
        }

        container.appendChild(label);
    }

    if (description) {
        const div = document.createElement("div");

        div.textContent = description;

        container.appendChild(div);
    }

    if (explanation) {
        const div = document.createElement("div");

        div.textContent = explanation;

        container.appendChild(div);
    }

    return container;
}
