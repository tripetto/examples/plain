import * as Tripetto from "tripetto-collector";
import { ITextarea } from "tripetto-block-textarea";
import { Static } from "../static";

@Tripetto.node("tripetto-block-textarea")
export class Password extends Tripetto.NodeBlock<HTMLElement, ITextarea> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const slot = this.SlotAssert("value");
        const value = this.DataAssert<string>(instance, slot);
        const rendering = document.createElement("div");
        const textarea = document.createElement("textarea");

        textarea.setAttribute("type", "text");
        textarea.setAttribute("rows", "3");
        textarea.setAttribute("id", this.Node.Props.Id);

        if (Tripetto.F.IsFilledString(this.Node.Props.Placeholder)) {
            textarea.setAttribute("placeholder", this.Node.Props.Placeholder);
        }

        textarea.value = value.Value;

        textarea.addEventListener("propertychange", () => (value.Value = textarea.value));
        textarea.addEventListener("change", () => (value.Value = textarea.value));
        textarea.addEventListener("click", () => (value.Value = textarea.value));
        textarea.addEventListener("keyup", () => (value.Value = textarea.value));
        textarea.addEventListener("input", () => (value.Value = textarea.value));
        textarea.addEventListener("paste", () => (value.Value = textarea.value));
        textarea.addEventListener("blur", () => (textarea.value = value.String));

        rendering
            .appendChild(
                Static(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    slot.Required,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(textarea);

        return rendering;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("value");
        const value = this.DataAssert<string>(instance, slot);

        return !slot.Required || value.Value !== "";
    }
}
