import * as Tripetto from "tripetto-collector";
import { IDropdown, IDropdownOption } from "tripetto-block-dropdown";
import { Static } from "../static";
import "./condition";

@Tripetto.node("tripetto-block-dropdown")
export class Dropdown extends Tripetto.NodeBlock<HTMLElement, IDropdown> {
    private Update(data: Tripetto.Data<string>, id: string | undefined): void {
        const value = Tripetto.F.FindFirst(this.Props.Options, (option: IDropdownOption) => option.Id === id);

        data.Set(value ? value.Value || value.Name : undefined, id);
    }

    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const slot = this.SlotAssert("option");
        const dropdown = this.DataAssert<string>(instance, slot);
        const rendering = document.createElement("div");
        const select = document.createElement("select");

        select.setAttribute("id", this.Node.Props.Id);

        if (Tripetto.F.IsFilledString(this.Node.Props.Placeholder)) {
            const option = document.createElement("option");

            option.textContent = this.Node.Props.Placeholder;
            option.value = "";

            select.appendChild(option);
        }

        Tripetto.F.Each(this.Props.Options, (dropdownOption: IDropdownOption) => {
            const option = document.createElement("option");

            option.textContent = dropdownOption.Name;
            option.value = dropdownOption.Id;

            if (dropdown.Reference === dropdownOption.Id) {
                option.selected = true;
            }

            select.appendChild(option);
        });

        select.addEventListener("propertychange", () => this.Update(dropdown, select.value));
        select.addEventListener("change", () => this.Update(dropdown, select.value));
        select.addEventListener("blur", () => this.Update(dropdown, select.value));

        this.Update(dropdown, select.value);

        rendering
            .appendChild(
                Static(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    slot.Required,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(select);

        return rendering;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("option");
        const dropdown = this.DataAssert<string>(instance, slot);

        return !slot.Required || dropdown.Reference !== "";
    }
}
