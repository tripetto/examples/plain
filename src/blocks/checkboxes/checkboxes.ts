import * as Tripetto from "tripetto-collector";
import { ICheckbox, ICheckboxes } from "tripetto-block-checkboxes";
import { Static } from "../static";

@Tripetto.node("tripetto-block-checkboxes")
export class Checkboxes extends Tripetto.NodeBlock<HTMLElement, ICheckboxes> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const rendering = document.createElement("div");
        const list = document.createElement("div");

        Tripetto.F.Each(this.Props.Checkboxes, (checkbox: ICheckbox) => {
            const state = this.Data<boolean>(instance, checkbox.Id);
            const label = document.createElement("label");
            const input = document.createElement("input");

            input.setAttribute("type", "checkbox");

            if (state) {
                input.checked = state.Value;

                input.addEventListener("propertychange", () => (state.Value = input.checked));
                input.addEventListener("change", () => (state.Value = input.checked));
                input.addEventListener("click", () => (state.Value = input.checked));
            }

            label.appendChild(input);
            label.appendChild(document.createTextNode(checkbox.Name));
            list.appendChild(label);
        });

        rendering
            .appendChild(
                Static(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    false,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(list);

        return rendering;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        return true;
    }
}
